import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.Map;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

public class WindoofBot extends TelegramLongPollingBot {

	private String botUsername;
	private String botToken;
	private String git;
	private String dsgvo;

	public WindoofBot(String username, String token, String git, String dsgvo){
		this.botUsername = username;
		this.botToken = token;
		this.git = git;
		this.dsgvo = dsgvo;
	}

	@Override
	public String getBotUsername() {
		return this.botUsername;
	}

	@Override
	public String getBotToken() {
		return this.botToken;
	}

	@Override
	public void onUpdateReceived(Update update) {
		if (update.hasMessage() && update.getMessage().hasText()) {
			String text = update.getMessage().getText().toLowerCase();
			SendMessage message = new SendMessage();
			message.setChatId(update.getMessage().getChatId());
			//write everything in lowercase in the contains
			boolean send = false;
			if(text.equals("/license") || text.equals("/license"+this.getBotUsername())){
				String msg = "Welcome!\nThis bot is a program which is available under the MIT license at " + this.git;
				message.setText(msg);
				send = true;
			} else if(text.equals("/dsgvo") || text.equals("/dsgvo"+this.getBotUsername())) {
				String msg = this.dsgvo;
				message.setText(msg);
				send = true;
			}
			else {
				String replaced = text;
				if(text.contains("windows")){
					replaced = replaced.replace("windows", "windoof");
					send = true;
				}
				if(text.contains("photoshop")){
					replaced = replaced.replace("photoshop", "krita oder gimp");
					send = true;
				}
				message.setText("Meinst du:\n<code>" + replaced + "</code>?");
			}
			if(!send){
				return;
			}
			message.setParseMode("html");
			try {
				sendMessage(message); // Call method to send the message
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
	}

}

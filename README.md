# TelegramTriggerBots
This is a collection of telegram trigger bots and also the new repo for current updates on these bots.

## Bots
- [TelegramEntenBot](https://gitlab.com/BergiuTelegram/TgEntenBot)
- [TelegramHicksBot](https://gitlab.com/BergiuTelegram/TgHicksBot)
- [TelegramRMSBot](https://gitlab.com/BergiuTelegram/TgRMSBot)
- [TelegramWindoofBot](https://gitlab.com/BergiuTelegram/TgWindoofBot)

## Configuration
- Create a new TelegramBot with the [Botfather](https://telegram.me/botfather) for each of the above bots, that you would like to run
- Disable privacy settings for the bot and enable the bot in groups
- Write your Botname and your Token into the file `config/config.yml` (or in the shared volume if you use the docker installation) for each bot and enable them
	- **Don't use the same username and token for different bots!**

### Botfather Commands
This commands should be added to each bot in the [Botfather](https://telegram.me/botfather):
<code>
license - License
dsgvo - Privacy statement
</code>

## Docker
### Official build:
- [hub.docker.com/r/bergiu/tgtriggerbots](https://hub.docker.com/r/bergiu/tgtriggerbots/)

### Build:
1. Build
	- `docker build -t bergiu/tgtriggerbots:v1.0.0 .`
	- `docker build -t bergiu/tgtriggerbots:latest .`

### Installation:
1. Run the container a first time with a shared volume
	- `docker run --name tgtriggerbots -v $HOSTFOLDER:/bot/config -d bergiu/tgtriggerbots:latest`
	- don't forget to replace or set the $HOSTFOLDER
2. Configure the bot-config in the shared volume
3. Start the container again
	- `docker start tgtriggerbots`

### Upload
1. `docker login`
2. `docker tag bergiu/tgtriggerbots bergiu/tgtriggerbots`
3. `docker push bergiu/tgtriggerbots`

## Without docker
### Dependencies
```
sudo apt install openjdk-8-jdk
```

### Installation
```shell
git clone https://gitlab.com/BergiuTelegram/TgTriggerBots
./build
./run # to create the config folder and file-template
```

### Run
- `./run.sh`

### Development
- `source rc.sh`
